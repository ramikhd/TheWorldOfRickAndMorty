package fr.iut.theworldofrickandmorty.raclem.datasource


import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location

interface ILocationProvider : IDataProvider<Location>