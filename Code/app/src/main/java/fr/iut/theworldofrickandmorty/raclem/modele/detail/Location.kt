package fr.iut.theworldofrickandmorty.raclem.modele.detail

class Location(
    val id: Int,
    val name: String,
    val type: String,
    val dimension: String,
    val residents : Iterable<fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character>

)