package fr.iut.theworldofrickandmorty.raclem.datasource

interface IToModele<T> {
    suspend fun ToModele() : T
}