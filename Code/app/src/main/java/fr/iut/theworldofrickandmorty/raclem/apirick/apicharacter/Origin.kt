package fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter
@kotlinx.serialization.Serializable
data class Origin(
    val name: String,
    val url: String
)