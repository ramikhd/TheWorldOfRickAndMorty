package fr.iut.theworldofrickandmorty.raclem.fragments.cardview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import fr.iut.theworldofrickandmorty.raclem.R

class CardViewEpisode : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_view_episode)


        val nameTextView = findViewById<TextView>(R.id.text_name)
        val air_dateTextView = findViewById<TextView>(R.id.text_air_date)
        val episodeTextView = findViewById<TextView>(R.id.episode)



        nameTextView.text = "Pilot"
        air_dateTextView.text = "December 2, 2013"
        episodeTextView.text="S01E01"
    }
}