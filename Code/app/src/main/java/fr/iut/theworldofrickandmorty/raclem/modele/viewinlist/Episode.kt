package fr.iut.theworldofrickandmorty.raclem.modele.viewinlist

import java.util.*

class Episode(
    val id: Int,
    val name: String,
    val airDate: Date,
    val episode: String,

)
