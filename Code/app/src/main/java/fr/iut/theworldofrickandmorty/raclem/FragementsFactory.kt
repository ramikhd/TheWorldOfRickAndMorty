package fr.iut.theworldofrickandmorty.raclem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.*


class FragementsFactory(val list : Iterable<Character>): FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when(className){
            ListCharacters::class.java.name-> ListCharacters(list)
            else ->   super.instantiate(classLoader, className)
        }



}