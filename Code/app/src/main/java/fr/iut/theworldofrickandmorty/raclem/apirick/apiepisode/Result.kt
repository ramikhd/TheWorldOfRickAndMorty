package fr.iut.theworldofrickandmorty.raclem.apirick.apiepisode

import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Episode
@Suppress("PLUGIN_IS_NOT_ENABLED")
@kotlinx.serialization.Serializable
data class Result(
    val air_date: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
): IToModele<Episode> {
    override suspend fun ToModele(): Episode {
        TODO("Not yet implemented")
    }

}