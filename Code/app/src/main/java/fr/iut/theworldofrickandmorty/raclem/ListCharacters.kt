package fr.iut.theworldofrickandmorty.raclem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.theworldofrickandmorty.raclem.adapatateur.AdaptateurCharacter
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiCharacterProvider
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ListCharacters(val list : Iterable<Character>?=null) : Fragment(R.layout.activity_list_charactere), CoroutineScope {

    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private lateinit var listCharacters: Iterable<Character>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_list_charactere, container, false)
        val api = ApiCharacterProvider()
        val recycler = view.findViewById<RecyclerView>(R.id.recycleViewCharacter)
        recycler.layoutManager = LinearLayoutManager(activity)
        if(list!=null)
            recycler.adapter = AdaptateurCharacter(list.toList().toTypedArray())

        else {
            launch {
                listCharacters = withContext(Dispatchers.IO) {
                    api.GetDatas()
                }
                recycler.adapter = AdaptateurCharacter(listCharacters.toList().toTypedArray())
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val api = ApiCharacterProvider()
        val recycler = view.findViewById<RecyclerView>(R.id.recycleViewCharacter)
        recycler.layoutManager = LinearLayoutManager(activity)
        launch {
            listCharacters = withContext(Dispatchers.IO) {
                api.GetDatas()

            }

            recycler.adapter = AdaptateurCharacter(listCharacters.toList().toTypedArray())
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel() // cancel the job when the fragment is destroyed
    }
}
