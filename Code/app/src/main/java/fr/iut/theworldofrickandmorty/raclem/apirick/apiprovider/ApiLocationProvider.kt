package fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider

import android.util.Log
import fr.iut.theworldofrickandmorty.raclem.apirick.apilocation.ApiResponseLocation
import fr.iut.theworldofrickandmorty.raclem.datasource.ILocationProvider
import fr.iut.theworldofrickandmorty.raclem.genericity.GetData
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location
import  fr.iut.theworldofrickandmorty.raclem.apirick.apilocation.Result

class ApiLocationProvider : ILocationProvider {
    val url : String = "https://rickandmortyapi.com/api/location"

    override suspend fun GetDatas(): Iterable<Location> {
        return  GetData<Iterable<Location>, ApiResponseLocation>(url);
    }

    override suspend fun GetData(id : Int): Location {
        return  GetData<Location, Result>("$url/$id");
    }
}