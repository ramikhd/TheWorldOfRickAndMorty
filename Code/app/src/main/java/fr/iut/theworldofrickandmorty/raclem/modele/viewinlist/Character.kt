package fr.iut.theworldofrickandmorty.raclem.modele.viewinlist

import fr.iut.theworldofrickandmorty.raclem.modele.Gender
import fr.iut.theworldofrickandmorty.raclem.modele.Status

class Character(
    val id: Int,
    val name: String,
    val status: Status,
    val species: String,
    val type: String,
    val image: String,
    val gender: Gender,
    val link : String,
)