package fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter
@kotlinx.serialization.Serializable
data class Location(
    val name: String,
    val url: String
)