package fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter

import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import fr.iut.theworldofrickandmorty.raclem.genericity.enumByNameIgnoreCase
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character
import fr.iut.theworldofrickandmorty.raclem.modele.Gender
import fr.iut.theworldofrickandmorty.raclem.modele.Status


@kotlinx.serialization.Serializable
data class Result(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: Location,
    val name: String,
    val origin: Origin,
    val species: String,
    val status: String,
    val type: String,
    val url: String
): IToModele<Character> {
    override suspend fun ToModele(): Character {
        return Character(id,name,enumByNameIgnoreCase<Status>(status)?: Status.UNKNOWN ,species,type,image,enumByNameIgnoreCase<Gender>(gender)?: Gender.UNKNOWN ,url)
    }
}