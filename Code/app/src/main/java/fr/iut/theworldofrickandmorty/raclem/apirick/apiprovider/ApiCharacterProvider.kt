package fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider

import fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter.ApiResponseCharacter
import fr.iut.theworldofrickandmorty.raclem.datasource.ICharacterProvider
import fr.iut.theworldofrickandmorty.raclem.genericity.GetData
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character
import fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter.Result

class ApiCharacterProvider : ICharacterProvider {
    val url : String = "https://rickandmortyapi.com/api/character"

    override suspend fun GetDatas(): Iterable<Character> {
        return  GetData<Iterable<Character>, ApiResponseCharacter>(url);
    }

    override suspend fun GetData(id : Int): Character {
        return  GetData<Character, Result>("$url/$id");
    }
}