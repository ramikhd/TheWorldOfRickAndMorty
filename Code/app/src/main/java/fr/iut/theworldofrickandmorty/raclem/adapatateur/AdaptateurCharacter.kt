package fr.iut.theworldofrickandmorty.raclem.adapatateur

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.iut.theworldofrickandmorty.raclem.R
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character

class AdaptateurCharacter(private val dataSet: Array<Character>) :
    RecyclerView.Adapter<AdaptateurCharacter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView = view.findViewById<TextView>(R.id.text_name)
        val genderTextView = view.findViewById<TextView>(R.id.gender)
        val speciesTextView = view.findViewById<TextView>(R.id.text_species)
        val statusTextView = view.findViewById<TextView>(R.id.text_status)
        val characterImage = view.findViewById<ImageView>(R.id.image_character)
        val typeTextView = view.findViewById<TextView>(R.id.type)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        Log.i("MyTag", "Data from server")
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.activity_card_view_character, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        Log.i("MyTag", "Data from server: ${dataSet[position].name}")

        viewHolder.nameTextView.text = dataSet[position].name
        Glide.with(viewHolder.itemView)
            .load(dataSet[position].image)
            .into(viewHolder.characterImage)
        viewHolder.statusTextView.text = dataSet[position].status.name
        viewHolder.genderTextView.text = dataSet[position].gender.name
        viewHolder.speciesTextView.text = dataSet[position].species
        viewHolder.typeTextView.text = dataSet[position].type
    }

    override fun getItemCount() = dataSet.size


}

