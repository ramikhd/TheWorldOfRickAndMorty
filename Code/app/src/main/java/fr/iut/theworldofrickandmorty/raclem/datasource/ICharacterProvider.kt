package fr.iut.theworldofrickandmorty.raclem.datasource
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character

interface ICharacterProvider : IDataProvider<Character>