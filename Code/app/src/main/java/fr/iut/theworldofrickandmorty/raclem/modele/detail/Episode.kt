package fr.iut.theworldofrickandmorty.raclem.modele.detail

import java.util.*

class Episode(
    val id: Int,
    val name: String,
    val airDate: Date,
    val episode: String,
    val residents : Iterable<fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character>

)
