package fr.iut.theworldofrickandmorty.raclem.modele

enum class Gender {
    FEMALE,
    MALE,
    GENDERLESS,
    UNKNOWN
}

enum class Status {
    ALIVE,
    DEAD,
    UNKNOWN
}