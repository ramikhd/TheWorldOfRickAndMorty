package fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter
import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Character
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location

@kotlinx.serialization.Serializable
data class ApiResponseCharacter(
    val info: Info,
    val results: List<Result>
): IToModele<Iterable<Character>> {
    override suspend fun ToModele(): Iterable<Character>  {
        val finalList = results.map { res -> res.ToModele() }
        return finalList
    }

}