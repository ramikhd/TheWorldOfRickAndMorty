package fr.iut.theworldofrickandmorty.raclem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.theworldofrickandmorty.raclem.adapatateur.AdaptateurLocation
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiLocationProvider
import fr.iut.theworldofrickandmorty.raclem.genericity.loadFromFile
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
class ListLocations : Fragment(), CoroutineScope {
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private lateinit var listLocations: Iterable<Location>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_list_locations, container, false)

        job = Job()

        val api = ApiLocationProvider()
        val recycler = view.findViewById<RecyclerView>(R.id.recycleViewLocation)
        recycler.layoutManager = LinearLayoutManager(context)
        launch {
            listLocations = withContext(Dispatchers.IO) {
                val fileName = "locations.json"
                 loadFromFile(requireContext(), fileName).toMutableList()
            }
            recycler.adapter = AdaptateurLocation(listLocations.toList().toTypedArray())
        }

        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}