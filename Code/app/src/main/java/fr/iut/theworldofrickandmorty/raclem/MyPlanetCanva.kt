package fr.iut.theworldofrickandmorty.raclem

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import kotlin.math.sqrt


class MyPlanetCanva(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    var PlanetSize:Float

    var PlanetName: String
    var ShowText: Boolean
    var TextSize: Float
    var TextColor: Int
    var paint : Paint

    constructor(context: Context) : this(context,null)
    init {



        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.PlanetCanva,
            0, 0
        ).apply {
            try {
                PlanetSize=getFloat(R.styleable.PlanetCanva_PlanetSize,10F)
                ShowText = getBoolean(R.styleable.PlanetCanva_ShowText, true)
                PlanetName = getString(R.styleable.PlanetCanva_PlanetName)?: "Planet"


                TextColor=getColor(R.styleable.PlanetCanva_TextColor,Color.WHITE)
                TextSize=getFloat(R.styleable.PlanetCanva_TextSize,5f)
            } finally {
                recycle()
            }
        }
        paint= PaintPlanet(width,height,PlanetSize)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        var xpad = (paddingLeft + paddingRight).toFloat()
        if (ShowText) xpad += TextSize
    }

    fun RefrechPaint(){
        paint=PaintPlanet(width,height,PlanetSize)
        textPaint= PaintText(TextSize,TextColor)
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val actualPlanetSize=(2f*PlanetSize).toInt()

        val listMeasureMaxWidth=mutableListOf(actualPlanetSize)
        val listMeasureMaxHeight=mutableListOf(actualPlanetSize*2)

        if(ShowText) {
            listMeasureMaxHeight.add(TextSize.toInt())
            listMeasureMaxWidth.add(TextSize.toInt() * PlanetName.length - 1)
        }

        val maxSizeWidth=listMeasureMaxWidth.max()
        val maxSizeHeight=listMeasureMaxHeight.sum()

        val minw: Int = paddingLeft + paddingRight + maxSizeWidth
        val w: Int = resolveSizeAndState(minw, widthMeasureSpec, 1)

        val minh: Int = maxSizeHeight
        val h: Int = resolveSizeAndState(minh, heightMeasureSpec, 1)

        setMeasuredDimension(w, h)
    }


    private var textPaint = PaintText(TextSize, TextColor)


    public override fun onDraw(canvas: Canvas) {

        super.onDraw(canvas)
        val actualPlanetSize=(2f*PlanetSize).toInt()

        val listMeasureMaxWidth=mutableListOf(actualPlanetSize)
        val listMeasureMaxHeight=mutableListOf(actualPlanetSize)

        if(ShowText) {
            listMeasureMaxHeight.add(TextSize.toInt())
            listMeasureMaxWidth.add(TextSize.toInt() * PlanetName.length - 1)
        }

        val maxSizeHeight=listMeasureMaxHeight.sum()
        canvas.apply {
            if(ShowText)
                drawText(PlanetName, 0f, (2*PlanetSize)+actualPlanetSize, textPaint)

            if(ShowText)
                canvas.drawCircle( (maxSizeHeight/2).toFloat(), actualPlanetSize.toFloat(), PlanetSize, paint);
            else
                canvas.drawCircle(0f, actualPlanetSize.toFloat(), PlanetSize, paint);
        }
    }


}