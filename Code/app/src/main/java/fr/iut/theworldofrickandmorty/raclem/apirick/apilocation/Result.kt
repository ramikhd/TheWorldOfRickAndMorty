package fr.iut.theworldofrickandmorty.raclem.apirick.apilocation

import android.util.Log
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiCharacterProvider
import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location

@kotlinx.serialization.Serializable
data class Result(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
): IToModele<Location> {
    override suspend fun ToModele(): Location {
        return Location(id, name, type, dimension)
    }
}