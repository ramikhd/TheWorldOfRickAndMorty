package fr.iut.theworldofrickandmorty.raclem.apirick.apilocation
import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location

import kotlinx.serialization.Serializable

@Serializable
data class ApiResponseLocation (
    val info: Info,
    val results: List<Result>
): IToModele<Iterable<Location>> {
    override suspend fun ToModele(): Iterable<Location> {
        val finalList = results.map { res -> res.ToModele() }
        return finalList
    }
}