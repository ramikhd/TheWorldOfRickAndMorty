package fr.iut.theworldofrickandmorty.raclem.datasource

interface IDataProvider<T> {

    suspend fun GetDatas(): Iterable<T>
    suspend fun GetData(id : Int): T

}
