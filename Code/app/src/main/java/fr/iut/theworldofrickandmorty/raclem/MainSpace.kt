package fr.iut.theworldofrickandmorty.raclem

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController

import android.os.Bundle
import android.content.res.Resources
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiLocationProvider
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

class MainSpace : Fragment(), SensorEventListener, CoroutineScope {

    private lateinit var job: Job
    private lateinit var sensorManager: SensorManager
    private var mLight: Sensor? = null

    private var IsSelected = false

    private var planets: MutableList<MyPlanetCanva> = mutableListOf()
    private lateinit var listLocations: Iterable<Location>
    private lateinit var viewRami:View
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


         viewRami = inflater.inflate(R.layout.activity_space_main, container, false)

        return viewRami


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        job = Job()
        val context = requireContext().applicationContext
        sensorManager = requireActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mLight = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        val api = ApiLocationProvider()
        launch {
            listLocations = withContext(Dispatchers.IO) {
                api.GetDatas()
            }
            val random = Random()
            val maxPlanetSize = 75
            val minPlanetSize = 50

            listLocations.forEach { location ->
                val planetSize = random.nextInt(maxPlanetSize - minPlanetSize) + minPlanetSize
                val planetX = random.nextInt(Resources.getSystem().displayMetrics.widthPixels * 2 - planetSize * 2)
                val planetY = random.nextInt(Resources.getSystem().displayMetrics.heightPixels * 2 - planetSize * 2)

                val planet = MyPlanetCanva(context, null)
                planet.PlanetName = location.name
                planet.PlanetSize = planetSize.toFloat()
                planet.TextSize = 35f
                planet.ShowText = true
                planet.TextColor = Color.WHITE

                planet.translationX = planetX.toFloat()
                planet.translationY = planetY.toFloat()
                planet.RefrechPaint()

                val layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )

                planet.layoutParams = layoutParams
                planets.add(planet)

                (view as ViewGroup).addView(planet, layoutParams)

            }

        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        // Do something here if sensor accuracy changes.
    }

    override fun onSensorChanged(event: SensorEvent) {
        val shipX=(Resources.getSystem().displayMetrics.widthPixels/2).toFloat()
        val shipY=(Resources.getSystem().displayMetrics.heightPixels/2).toFloat()-230
        if (event.sensor?.type == Sensor.TYPE_GYROSCOPE) {
            val random = Random()
            planets.forEach { planet ->
                val randomX = random.nextInt(10) - random.nextInt(10)
                val randomY = random.nextInt(10) - random.nextInt(10)

                planet.translationX = planet.translationX + (event.values[1] * 250) + randomX
                planet.translationY = planet.translationY + (event.values[0] * 250) + randomY
                if (planet.translationX+planet.PlanetSize*2 >= shipX   &&
                    planet.translationX <= shipX+planet.PlanetSize*2  &&
                    planet.translationY+planet.PlanetSize*2 >= shipY &&
                    planet.translationY <= shipY+planet.PlanetSize*2 &&
                    ! IsSelected
                     ) {
                    IsSelected=true
                    val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    val idToSave=listLocations.find { plant-> plant.name==planet.PlanetName }!!.id
                    Log.i("idToSave","$idToSave")
                    editor.putInt("idPlanet", idToSave).apply()

                    this.findNavController().navigate(R.id.detailLocation)


                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mLight?.also { light ->
            sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
        }

    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

}
