package fr.iut.theworldofrickandmorty.raclem.datasource
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Episode

interface IEpisodeProvider : IDataProvider<Episode>
