package fr.iut.theworldofrickandmorty.raclem.adapatateur

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.iut.theworldofrickandmorty.raclem.R
import fr.iut.theworldofrickandmorty.raclem.fragments.cardview.CardViewLocation
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location

class AdaptateurLocation(private val dataSet: Array<Location>) :
    RecyclerView.Adapter<AdaptateurLocation.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val nameTextView = view.findViewById<TextView>(R.id.text_name)
        val dimentionTextView = view.findViewById<TextView>(R.id.text_dimention)
        val typeTextView = view.findViewById<TextView>(R.id.type)


    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        Log.i("MyTag", "Data from server")
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.activity_card_view_location, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        Log.i("MyTag", "Data from server: ${dataSet[position].name}")

        viewHolder.nameTextView.text = dataSet[position].name

        viewHolder.dimentionTextView.text = dataSet[position].dimension
        viewHolder.typeTextView.text = dataSet[position].type
    }

    override fun getItemCount() = dataSet.size


}

