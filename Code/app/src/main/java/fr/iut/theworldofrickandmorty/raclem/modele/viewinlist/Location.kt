package fr.iut.theworldofrickandmorty.raclem.modele.viewinlist

data class Location(
    val id: Int,
    val name: String,
    val type: String,
    val dimension: String,
)