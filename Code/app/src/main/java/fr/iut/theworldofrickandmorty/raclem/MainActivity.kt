package fr.iut.theworldofrickandmorty.raclem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.commit
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiCharacterProvider
import kotlinx.coroutines.*
import androidx.navigation.fragment.findNavController
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, MainSpace())
                .commit()
        }
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        bottomNavigationView.setOnItemSelectedListener { item ->

            when (item.itemId) {
                R.id.space -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, MainSpace())
                        .commit()
                    true
                }

                R.id.characters -> {


                    job = Job()
                    val api = ApiCharacterProvider()
                    launch {
                        val listCharacters = withContext(Dispatchers.IO) {
                            api.GetDatas()
                        }
                        supportFragmentManager.fragmentFactory= FragementsFactory(listCharacters)
                        supportFragmentManager.commit { replace(R.id.fragment_container,ListCharacters::class.java,null) }

                        findNavController(R.id.fragment_container).navigate(R.id.listCharacters)
                    }

                    true
                }
                 R.id.favorites -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, ListLocations())
                        .commit()
                    true
                }

                else -> false
            }
        }
    }
}