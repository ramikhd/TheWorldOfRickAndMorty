package fr.iut.theworldofrickandmorty.raclem.fragments.cardview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import fr.iut.theworldofrickandmorty.raclem.FragementsFactory
import fr.iut.theworldofrickandmorty.raclem.R
import fr.iut.theworldofrickandmorty.raclem.modele.detail.Location

class CardViewLocation(val location:Location) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_card_view_location, container, false)

        val nameTextView = view.findViewById<TextView>(R.id.text_name)
        val dimentionTextView = view.findViewById<TextView>(R.id.text_dimention)
        val typeTextView = view.findViewById<TextView>(R.id.type)

        nameTextView.text = location.name
        dimentionTextView.text =  location.dimension
        typeTextView.text=location.type
        return view
    }
}