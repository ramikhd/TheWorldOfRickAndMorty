package fr.iut.theworldofrickandmorty.raclem.apirick.apicharacter

@kotlinx.serialization.Serializable
data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: String?
)