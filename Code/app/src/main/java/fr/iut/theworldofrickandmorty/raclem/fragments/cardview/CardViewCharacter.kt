package fr.iut.theworldofrickandmorty.raclem.fragments.cardview

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import fr.iut.theworldofrickandmorty.raclem.R

class CardViewCharacter : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_view_character)

        val imageView = findViewById<ImageView>(R.id.image_character)
        val nameTextView = findViewById<TextView>(R.id.text_name)
        val statusTextView = findViewById<TextView>(R.id.text_status)
        val speciesTextView = findViewById<TextView>(R.id.text_species)
        val typeTextView = findViewById<TextView>(R.id.type)
        val genderTextView = findViewById<TextView>(R.id.gender)

        Glide.with(this)
            .load("https://rickandmortyapi.com/api/character/avatar/361.jpeg")
            .into(imageView)

        nameTextView.text = "Toxic Rick"
        statusTextView.text = "Dead"
        speciesTextView.text = "Humanoid"
        typeTextView.text="Rick's Toxic Side"
        genderTextView.text="Male"


    }
}