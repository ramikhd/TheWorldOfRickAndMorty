package fr.iut.theworldofrickandmorty.raclem.fragments.detailview
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import fr.iut.theworldofrickandmorty.raclem.R
import fr.iut.theworldofrickandmorty.raclem.apirick.apiprovider.ApiLocationProvider
import fr.iut.theworldofrickandmorty.raclem.genericity.loadFromFile
import fr.iut.theworldofrickandmorty.raclem.genericity.saveToFile
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailPlanet : Fragment() {

    private lateinit var saveButton: Button
    private lateinit var location: Location
    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        id = sharedPreferences.getInt("idPlanet", 0)
        Log.i("idSaved","$id")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_detail_planet, container, false)
        saveButton = view.findViewById(R.id.save_location_button)
        saveButton.setOnClickListener { saveLocationToStorage() }
        val coroutineScope = CoroutineScope(Dispatchers.Main)

        val api = ApiLocationProvider()
        coroutineScope.launch {
            location = api.GetData(id)
            val nameTextView = view.findViewById<TextView>(R.id.text_name)
            val dimentionTextView = view.findViewById<TextView>(R.id.text_dimention)
            val typeTextView = view.findViewById<TextView>(R.id.type)
            nameTextView.text=location.name
            dimentionTextView.text=location.dimension
            typeTextView.text=location.type

        }

        return view
    }

    private fun saveLocationToStorage() {
        // Load existing locations from file
        val fileName = "locations.json"
        val existingLocations: MutableList<Location> = loadFromFile(requireContext(), fileName).toMutableList()

        // Check if location is already saved

        if (existingLocations.any { it.name == location.name }) {
            Toast.makeText(requireContext(), "Location already saved", Toast.LENGTH_SHORT).show()
            return
        }


        // Add new location to list
        existingLocations.add(location)
        saveToFile(requireContext(), fileName, existingLocations)
        Toast.makeText(requireContext(), "Location saved", Toast.LENGTH_SHORT).show()
    }
}
