@startuml
skinparam classAttributeIconSize 0

    Enum Gender{
     Female,
     Male, 
     Genderless,
     unknown

    }

   Enum Status{
     Alive,
     Dead,
     unknown
    }  


   Character  "location" --> "1" Location
   Character  "origin " --> "1" Location
   Character  "episode" --> "1" Episode
   Character  "status " --> "1" Status
   Character  "gender" --> "1" Gender


    class Character{
        -id : int
        -name : string
        -status : Status
        -species: string
        -type : string
        -image : string

    }

    Location"residents " --> "*" Character
    class Location{
        -id : int
        -name : string
        -type : string
        -dimension : string
    }

    Episode "charachtersShowned " --> "*" Character
    class Episode{
        -id : int
        -name : string
        -airDate : Date
        -episode : string
    }


@enduml