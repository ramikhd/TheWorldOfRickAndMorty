package fr.iut.theworldofrickandmorty.raclem

import android.graphics.Color
import android.graphics.Paint
import android.graphics.RadialGradient
import android.graphics.Shader
import android.util.Log

fun PaintText( TextSize: Float,TextColor : Int):Paint {
    val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = TextColor
        Log.i("Draw : ", "${TextSize} ")
        textSize = TextSize

    }
    return paint
}

fun PaintPlanet( width: Int,height : Int,PlanetSize:Float): Paint {
    val color1: Int = Color.argb(
        255,
        (Math.random() * 256).toInt(),
        (Math.random() * 256).toInt(),
        (Math.random() * 256).toInt()
    )
    val color2: Int = Color.argb(
        255,
        (Math.random() * 256).toInt(),
        (Math.random() * 256).toInt(),
        (Math.random() * 256).toInt()
    )
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    paint.style = Paint.Style.FILL
    paint.shader =
        RadialGradient(
            (width / 2).toFloat(), (height / 2).toFloat(), PlanetSize, color1, color2, Shader.TileMode.MIRROR
        )
    return paint
}





