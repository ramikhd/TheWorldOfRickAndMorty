package fr.iut.theworldofrickandmorty.raclem.genericity

import android.content.Context
import android.util.Log
import fr.iut.theworldofrickandmorty.raclem.datasource.IToModele
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fr.iut.theworldofrickandmorty.raclem.modele.viewinlist.Location
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

suspend inline fun<T, reified R : IToModele<T> > GetData  (url: String) : T {
    try {

        val client = OkHttpClient()
        val request = Request.Builder().url(url).build()

        val response = withContext(Dispatchers.IO) {
            client.newCall(request).execute().body()?.string()
        }
        val rep = Json.decodeFromString<R>(response.toString())
        return rep.ToModele()

    } catch (e: IOException) {
        throw Exception("Network error", e)
    } catch (e: Exception) {
        throw Exception("Error", e)
    }
}

inline fun <reified T : Enum<T>> enumByNameIgnoreCase(input: String, default: T? = null): T? {
    return enumValues<T>().firstOrNull { it.name.equals(input, true) } ?: default
}

/*
fun <T>deserializeFromJson(jsonString: String): T {
    val gson = Gson()
    val type = object : TypeToken<T>() {}.type
    Log.i("general","${type.typeName}")
    return gson.fromJson(jsonString, type)
}

fun <T> serializeToJson(data: T): String {
    val gson = Gson()
    return gson.toJson(data)
}

fun <T> saveToFile(context: Context, fileName: String, data: T) {
    val jsonString = serializeToJson(data)
    val file = File(context.filesDir, fileName)
    file.writeText(jsonString)
}

fun <T> loadFromFile(context: Context, fileName: String):T? {
    val file = File(context.filesDir, fileName)
    if (!file.exists()) {
        return null
    }
    val jsonString = file.readText()
    return deserializeFromJson<T>(jsonString)
}*/

fun serializeLocationsToJson(locations: List<Location>): String {
    val gson = Gson()
    return gson.toJson(locations)
}
fun deserializeLocationsFromJson(jsonString: String): List<Location> {
    val gson = Gson()
    val type = object : TypeToken<List<Location>>() {}.type
    return gson.fromJson(jsonString, type)
}

fun saveToFile(context: Context, fileName: String, locations: List<Location>) {
    val jsonString = serializeLocationsToJson(locations)
    val file = File(context.filesDir, fileName)
    file.writeText(jsonString)
}


fun loadFromFile(context: Context, fileName: String): List<Location> {
    val file = File(context.filesDir, fileName)
    if (file.exists()) {
        val jsonString = file.readText()
        return deserializeLocationsFromJson(jsonString)
    }
    return emptyList()
}