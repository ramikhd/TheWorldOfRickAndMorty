package fr.iut.theworldofrickandmorty.raclem.apirick.apiepisode
@Suppress("PLUGIN_IS_NOT_ENABLED")
@kotlinx.serialization.Serializable
data class ApiResponseEpisode(
    val info: Info,
    val results: List<Result>
)